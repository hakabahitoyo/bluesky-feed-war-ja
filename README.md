# Bluesky Feed War Japanese

日本語での戦争の話題を拾うBlueskyのフィードです。SkyFeedを使わずに、独立して運用されています。

パブリッシュされたフィードは https://bsky.app/profile/did:plc:6544azsie6m6rtevnd63comq/feed/war-ja にあります。

このリポジトリは https://github.com/bluesky-social/feed-generator のフォークです。

このフィードを作る過程での作業を https://gitlab.com/hakabahitoyo/bluesky-feed-creation-memo に記録しています。

## メモ

- https://bsky.social/xrpc/com.atproto.identity.resolveHandle?handle=fubaiundo.org
- https://internect.info/
