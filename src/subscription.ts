import {
  OutputSchema as RepoEvent,
  isCommit,
} from './lexicon/types/com/atproto/sync/subscribeRepos'
import { FirehoseSubscriptionBase, getOpsByType } from './util/subscription'

import fs from 'node:fs'

export class FirehoseSubscription extends FirehoseSubscriptionBase {
  async handleEvent(evt: RepoEvent) {
    if (!isCommit(evt)) return

    const ops = await getOpsByType(evt)

    const postsToDelete = ops.posts.deletes.map((del) => del.uri)
    const postsToCreate = ops.posts.creates
      .filter((create) => {
        // only war-ja-related posts
        const lower = create.record.text.toLowerCase()

		/* Keywords */
        const isWarJa =
          lower.includes('ウクライナ') ||
          (lower.includes('ロシア') &&
            !lower.includes('ロシアンブルー') &&
            !lower.includes('ロシアンルーレット') &&
            !lower.includes('コロシアム') &&
            !lower.includes('アーリャさん') &&
            !lower.includes('アンブロシア') &&
            !lower.includes('ロシデレ') &&
            !lower.includes('コロシアイ') &&
            !lower.includes('ロシア人のチ◯コ') &&
            !lower.includes('むっつりドスケベロシア義母姉妹の本質見抜いてセックス三昧') &&
            !lower.includes('ロシアンツイスト') &&
            !lower.includes('ロシアンレンズ') &&
            !lower.includes('ロシアンたこ焼き') &&
            !lower.includes('ロシアンたこやき') &&
            !lower.includes('ヨーグルトの日') &&
            !lower.includes('デレる') &&
            !lower.includes('ロシアン佐藤') &&
            !lower.includes('缶詰はナポレオンのロシア遠征のために開発された') &&
            !lower.includes('ドロシア')
          ) ||
          lower.includes('ゼレンスキー') ||
          (lower.includes('プーチン')
            && !lower.includes('ラスプーチン')) ||
          (lower.includes('ハマス')
            && !lower.includes('ハマスタ')
            && !lower.includes('ハマスホイ')
            && !lower.includes('ハマスカ')
            && !lower.includes('ハマスゲ')
          ) ||
          lower.includes('イスラエル') ||
          (lower.includes('ガザ')
            && !lower.includes('テナガザル')
            && !lower.includes('ガザニア')
            && !lower.includes('ガザガザ')
            && !lower.includes('メガザル')
            && !lower.includes('オナガザル')
            && !lower.includes('ガザg')
            && !lower.includes('ガザミ')
            && !lower.includes('ヌガザカ')
            && !lower.includes('ハマスゲ')
            && !lower.includes('ガザガサ')
            && !lower.includes('ガザガザ')
            && !lower.includes('ガザc')
            && !lower.includes('ガザニガ')
          ) ||
          lower.includes('パレスチナ') ||
          lower.includes('ネタニヤフ') ||
          lower.includes('ハニヤ') ||
          lower.includes('ヒズボラ') ||
          (lower.includes('スーダン')
            && !lower.includes('スーダン2')) ||
          lower.includes('ダルフール') ||
          lower.includes('ハルツーム') ||
          lower.includes('即応支援部隊') ||
          lower.includes('ミャンマー') ||
          lower.includes('シャン州') ||
          lower.includes('国民統一政府') ||
          lower.includes('ミャンマー民族民主同盟軍') ||
          lower.includes('タアン民族解放軍') ||
          lower.includes('アラカン軍') ||
          lower.includes('フーシ派') ||
          lower.includes('イエメン') ||
          lower.includes('イラクのイスラム抵抗運動') ||
          lower.includes('イラクのイスラーム抵抗運動') ||
          lower.includes('ハマース') ||
          lower.includes('レバノン') ||
          (lower.includes('シリア')
            && !lower.includes('エクシリア')
            && !lower.includes('セシリア')
            && !lower.includes('シリアス')
            && !lower.includes('シリアル')
            && !lower.includes('マッシリア')
            && !lower.includes('シリアライズ')
            && !lower.includes('キシリア')
            && !lower.includes('シリアライザ')
            && !lower.includes('シリアライズ')
            && !lower.includes('シリアーティ')
            && !lower.includes('シリアライゼーション')
            && !lower.includes('シリアナ')
            && !lower.includes('シシリア')
            && !lower.includes('アッシリア')
            && !lower.includes('ハシリアン')
            && !lower.includes('シリアゲ')
            && !lower.includes('シリアリ')
            && !lower.includes('アシリア')
          ) ||
          (lower.includes('アサド')
            && !lower.includes('ハビエル')
          ) ||
          lower.includes('ハヤト・タハリール・アル・シャーム') ||
          lower.includes('シャーム解放機構') ||
          (lower.includes('コンゴ')
            && !lower.includes('コンゴウ')
            && !lower.includes('コンゴトモ')
          ) ||
          lower.includes('3月23日運動') ||
          lower.includes('ルワンダ')

    /* Denial keywords */
    const isNotWarJa =
      lower.includes('4ndan.com') ||
      lower.includes('及川幸久') ||
      lower.includes('gofund.me') ||
      lower.includes('gofundme.com') ||
      lower.includes('櫻井よしこ') ||
      lower.includes('高市早苗') ||
      lower.includes('youtube.com') ||
      lower.includes('youtu.be') ||
      lower.includes('申し訳ありませんが、この方法で連絡させていただきます。') ||
      lower.includes('寄付はプロフィールのリンクから行っていただけます。') ||
      lower.includes('子供の尻を叩く事が禁止されている')

		/* User ban */
        const did = create.author
		let isBannedUser = true

		try {
			const fileContent = fs.readFileSync('bans.json', 'utf8')
			const bans = JSON.parse(fileContent)
			isBannedUser = bans.includes(did)
		} catch (err) {
			console.error(err)
		}

         return isWarJa && (!isNotWarJa) && (!isBannedUser)
      })
      .map((create) => {
        // map war-ja-related posts to a db row
        return {
          uri: create.uri,
          cid: create.cid,
          indexedAt: new Date().toISOString(),
        }
      })

    if (postsToDelete.length > 0) {
      await this.db
        .deleteFrom('post')
        .where('uri', 'in', postsToDelete)
        .execute()
    }
    if (postsToCreate.length > 0) {
      await this.db
        .insertInto('post')
        .values(postsToCreate)
        .onConflict((oc) => oc.doNothing())
        .execute()
    }
  }
}
